from django.urls import path
from . import views

urlpatterns = [
    path('atur-distribusi/', views.atur_distribusi),
    path('daftar-distribusi/', views.daftar_distribusi),
    path('detail-distribusi/', views.detail_distribusi),
    path('update-distribusi/', views.update_distribusi)
]