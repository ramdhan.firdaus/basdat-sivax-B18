from django.apps import AppConfig


class DistribusiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'distribusi'
