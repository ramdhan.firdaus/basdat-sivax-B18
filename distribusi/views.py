from django.shortcuts import render

# Create your views here.
def atur_distribusi(request):
    return render(request, 'atur_distribusi.html')

def daftar_distribusi(request):
    return render(request, 'daftar_distribusi.html')

def detail_distribusi(request):
    return render(request, 'detail_distribusi.html')

def update_distribusi(request):
    return render(request, 'update_distribusi.html')