from django.urls import path
from . import views

urlpatterns = [
    path('', views.sivax),
    path('home/', views.home),
    path('home-panitia/', views.home_panitia),
    path('home-admin/', views.home_admin),
]
