from django.urls import path
from . import views

urlpatterns = [
    path('', views.tiket_saya),
    path('detail/', views.detail_tiket),
]