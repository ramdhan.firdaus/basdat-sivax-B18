from django.shortcuts import render
from .models import *

# Create your views here.

def tiket_saya(request):
    return render(request, 'tiket_saya.html')

def detail_tiket(request):
    return render(request, 'detail_tiket.html')