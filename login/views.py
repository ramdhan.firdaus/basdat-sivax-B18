from django.shortcuts import render
from .models import *

# Create your views here.

def register(request):
    return render(request, 'register.html')

def register_warga(request):
    return render(request, 'register_warga.html')

def register_admin_sistem(request):
    return render(request, 'register_admin.html')

def register_panitia_penyelenggara(request):
    return render(request, 'register_panitia.html')

def login(request):
    return render(request, 'login.html')