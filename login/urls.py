from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('warga/', views.register_warga, name='warga'),
    path('admin-sistem/', views.register_admin_sistem, name='admin'),
    path('panitia/', views.register_panitia_penyelenggara, name='panitia'),
    path('login/', views.login, name='login'),
]