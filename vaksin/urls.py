from django.urls import path
from . import views

urlpatterns = [
    path('', views.vaksin),
    path('create/', views.create),
    path('jadwal-vaksinasi/', views.jadwal),
    path('daftar-vaksinasi/', views.daftar),
    path('update-vaksin', views.update),
    path('tambah-update', views.tambah_update)
]