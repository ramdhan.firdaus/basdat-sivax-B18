from django.shortcuts import render
from .models import *

# Create your views here.

def vaksin(request):
    return render(request, 'vaksin.html')

def create(request):
    return render(request, 'create_vaksin.html')

def jadwal(request):
    return render(request, 'jadwal.html')

def daftar(request):
    return render(request, 'daftar.html')

def update(request):
    return render(request, 'update.html')

def tambah_update(request):
    return render(request, 'tambah_update.html')