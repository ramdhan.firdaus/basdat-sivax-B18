from django.shortcuts import render

# Create your views here.
def tambah(request):
    return render(request, 'tambah.html')

def detail_penjadwalan(request):
    return render(request, 'detail_penjadwalan.html')

def detail_pengajuan(request):
    return render(request, 'detail_pengajuan.html')

def penjadwalan(request):
    return render(request, 'daftar_penjadwalan.html')

def pengajuan(request):
    return render(request, 'daftar_pengajuan.html')

def update_pengajuan(request):
    return render(request, 'update_pengajuan.html')

def verifikasi(request):
    return render(request, 'verifikasi.html')