from django.urls import path
from . import views

urlpatterns = [
    path('tambah/', views.tambah),
    path('update-pengajuan/', views.update_pengajuan),
    path('detail-penjadwalan/', views.detail_penjadwalan),
    path('detail-penjadwalan/', views.detail_penjadwalan),
    path('detail-pengajuan/', views.detail_pengajuan),
    path('pengajuan/', views.pengajuan),
    path('penjadwalan/', views.penjadwalan),
    path('verifikasi', views.verifikasi)
]